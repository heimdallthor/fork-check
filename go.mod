module fork-check

go 1.18

require (
	github.com/logrusorgru/aurora/v3 v3.0.0
	github.com/olekukonko/tablewriter v0.0.5
	github.com/urfave/cli/v2 v2.4.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
)
