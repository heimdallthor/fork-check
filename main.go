package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/logrusorgru/aurora/v3"
	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli/v2"
	"golang.org/x/sync/errgroup"
	"golang.org/x/sync/semaphore"
)

var (
	port      int
	nodesFile string
)

func main() {
	app := &cli.App{
		Name: "fork-check",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "file",
				Aliases:     []string{"f"},
				Usage:       "json file which contains node address & ip address",
				Value:       "nodes.json",
				HasBeenSet:  true,
				Destination: &nodesFile,
			},
			&cli.IntFlag{
				Name:        "port",
				Aliases:     []string{"p"},
				Usage:       "port",
				EnvVars:     []string{"SERVER_PORT"},
				Value:       27147,
				HasBeenSet:  true,
				Destination: &port,
			},
		},
		Action: appAction,
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
func getNodes(ctx *cli.Context) ([]Node, error) {
	content, err := ioutil.ReadFile(nodesFile)
	if err != nil {
		return nil, fmt.Errorf("fail to read file: %s,err: %w", nodesFile, err)
	}
	type tmpNode struct {
		NodeAddress string `json:"node_address"`
		Status      string `json:"status"`
		IPAddress   string `json:"ip_address"`
	}
	var nodes []tmpNode
	if err := json.Unmarshal(content, &nodes); err != nil {
		return nil, fmt.Errorf("fail to parse nodes file: %s,err: %w", nodesFile, err)
	}
	var results []Node
	for _, item := range nodes {
		if item.Status == "Active" {
			results = append(results, Node{
				Address: item.NodeAddress,
				IP:      item.IPAddress,
			})
		}
	}
	return results, nil
}
func appAction(ctx *cli.Context) error {
	if nodesFile == "" {
		return fmt.Errorf("did not specify nodes file , please specify nodes file using --file")
	}
	if port == 0 {
		return fmt.Errorf("please specify port using --port or --p")
	}
	nodes, err := getNodes(ctx)
	if err != nil {
		return err
	}
	nodeChains, err := getNodesChain(ctx, nodes)
	if err != nil {
		return fmt.Errorf("fail to get chain ids, err: %w", err)
	}
	tw := tablewriter.NewWriter(os.Stdout)
	tw.SetBorder(true)
	tw.SetRowLine(true)
	tw.SetHeader([]string{
		"address",
		"IP",
		"chain_id",
		"status",
	})
	totalForked := 0
	for _, item := range nodeChains {
		var status aurora.Value
		if item.ChainID == "thorchain-mainnet-v1" {
			status = aurora.Green("OK")
			totalForked++
		} else {
			status = aurora.Red("NOT Forked")
		}
		tw.Append([]string{
			item.Address,
			item.IP,
			item.ChainID,
			status.String(),
		})
	}
	tw.Render()
	fmt.Printf("total: %d, forked: %d \n", len(nodeChains), totalForked)
	return nil
}
func getChainID(ip string) (string, error) {
	client := http.Client{Timeout: time.Second * 5}
	url := fmt.Sprintf("http://%s:%d/status", ip, port)
	res, err := client.Get(url)
	if err != nil {
		return "", fmt.Errorf("fail to get status from %s,err: %w", url, err)
	}
	if res.StatusCode != http.StatusOK {
		return "", fmt.Errorf("url: %s, status code: %d is not expected", url, res.StatusCode)
	}
	defer res.Body.Close()
	var status struct {
		Result struct {
			NodeInfo struct {
				Network string `json:"network"`
			} `json:"node_info"`
		} `json:"result"`
	}
	dec := json.NewDecoder(res.Body)
	if err := dec.Decode(&status); err != nil {
		return "", fmt.Errorf("fail to decode chain id,err: %w", err)
	}
	return status.Result.NodeInfo.Network, nil
}

func getNodesChain(ctx *cli.Context, nodes []Node) ([]NodeChain, error) {
	if len(nodes) == 0 {
		return nil, nil
	}
	var results []NodeChain
	lock := &sync.Mutex{}
	sem := semaphore.NewWeighted(10)
	grp, _ := errgroup.WithContext(ctx.Context)
	for _, item := range nodes {
		n := item
		grp.Go(func() error {
			if err := sem.Acquire(context.Background(), 1); err != nil {
				return fmt.Errorf("fail to acquire semaphore,err: %w", err)
			}
			defer sem.Release(1)
			chainID, err := getChainID(n.IP)
			if err != nil {
				return fmt.Errorf("fail to get chain id for %s,err: %w", n.IP, err)
			}
			lock.Lock()
			defer lock.Unlock()
			results = append(results, NodeChain{
				Node:    n,
				ChainID: chainID,
			})
			return nil
		})
	}

	if err := grp.Wait(); err != nil {
		return results, err
	}

	return results, nil
}
