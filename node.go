package main

// Node represent node account in thorchain
type Node struct {
	Address string `json:"address"`
	IP      string `json:"ip"`
}

// NodeChain represent Node with additional chain id
type NodeChain struct {
	Node
	ChainID string `json:"chain_id"`
}
